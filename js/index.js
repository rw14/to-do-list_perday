const TODOLIST_LOCALSTORAGE = "TODOLIST";
let todo = [];

const COMPLETEDLIST_LOCALSTORAGE = "COMPLETEDLIST";
let completed = [];

// check data dưới localStorage
let todoListJson = localStorage.getItem(TODOLIST_LOCALSTORAGE);
let completedListJson = localStorage.getItem(COMPLETEDLIST_LOCALSTORAGE);

if (todoListJson != null || completedListJson != null) {
  todo = JSON.parse(todoListJson).map((item) => item);
  completed = JSON.parse(completedListJson).map((item) => item);

  render();
}

// get in4
document.querySelector("#addItem").onclick = function () {
  let newTask = document.querySelector("#newTask").value;
  if (newTask == "") {
    alert("Enter something you need to do today ");
  } else {
    document.querySelector("#newTask").value = "";

    todo.push(newTask);

    render();
  }
};

// checked
function checked(name) {
  let index = completed.findIndex((item) => item === name);
  // get item ->add to completed
  //   let item = todo.slice(index, 1);
  let item = todo.splice(index, 1);

  completed.push(item);
  //   completed.splice(index, 1);
  render();
}

// delete item
function deleteList(name) {
  let index = todo.findIndex((item) => item === name);

  if (index != -1) {
    todo.splice(index, 1);
  }
  //   console.log(todo);
  //   console.log(completed);
  render();
}

// render data
function render() {
  let contentToDo = "";
  let contentCompleted = "";

  todo.forEach((item) => {
    contentToDo += /*html*/ `
        <li>
        <span>${item}</span>
        <div class="buttons">
          <button class="remove" style="color:red">
            <i class="fa-regular fa-trash-can" onclick="deleteList('${item}')"></i>
          </button>
          <button class="complete checked" onclick="checked('${item}')">
            <i class="fa-solid fa-circle-check"></i>
          </button>
        </div>
      </li>
      `;
  });
  document.querySelector("#todo").innerHTML = contentToDo;

  completed.forEach((item) => {
    contentCompleted += /*html*/ `
          <li>
          <span>${item}</span>
          <div class="buttons">
    
            <button class="complete checked" style="color:green">
              <i class="fa-solid fa-circle-check"></i>
            </button>
          </div>
        </li>
        `;
  });
  document.querySelector("#completed").innerHTML = contentCompleted;
  localStorage.setItem(TODOLIST_LOCALSTORAGE, JSON.stringify(todo));
  localStorage.setItem(COMPLETEDLIST_LOCALSTORAGE, JSON.stringify(completed));
}

document.querySelector("#one").onclick = function () {
  document.querySelector("#todo").classList.toggle("d-none");
};

document.querySelector("#two").onclick = function () {
  todo.sort();
  completed.sort();
  render();
};

document.querySelector("#three").onclick = function () {
  todo.sort();
  todo.reverse();
  completed.sort();
  completed.reverse();
  render();
};

document.querySelector("#all").onclick = function () {
  document.querySelector("#todo").classList.remove("d-none");
};
